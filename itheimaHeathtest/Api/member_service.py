
import json

#将会员服务封装类
from 封装_dubbo.base_service import BaseService


class MemberService(BaseService):
    def __init__(self):
        super().__init__()
        self.service_name = "MemberService"

    def find_by_tel(self, tel):
        resp = self.dubbo_client.invoke(self.service_name, "findByTelephone", tel)
        if resp == "null":
            return None
        # 作用：将 string类型的 数据，还原回成 字典 或 列表 数据。
        else:
            return json.loads(resp)

    def add(self, info):
        info["class"] = "com.itheima.pojo.Member"
        resp = self.dubbo_client.invoke(self.service_name, "add", info)
        if resp == "Faile":
            return None
        else:
            return json.loads(resp)

    def find_member_count_by_months(self, months):
        resp = self.dubbo_client.invoke(self.service_name, "findMemberCountByMonths", months)
        return json.loads(resp)


if __name__ == '__main__':
    ms = MemberService()
    resp = ms.find_by_tel("13020210001")
    print("响应结果 =", resp)
    print("type(resp) =", type(resp))
    print("=" * 20)

    # months = ["2021-6"]
    # ms = MemberService()
    # ms.find_member_count_by_months(months)
    # print("响应结果 =", resp)
    #
    # print("=" * 20)
    # info = {"id": 914, "name": "杜2", "phoneNumber": "16048379041"}
    # ms = MemberService()
    # resp = ms.add(info)
    # print("响应结果 =", resp)
    # print("type(resp) =", type(resp))
