from 封装_dubbo.base_service import BaseService


import json


class UserService(BaseService):
    def __init__(self):
        super().__init__()
        self.service_name = "UserService"

    def findByUsername(self, name):
        resp = self.dubbo_client.invoke(self.service_name, "findByUsername", name)
        if resp == "null":
            return None
        else:
            return json.loads(resp)


if __name__ == '__main__':
    name = "admin"
    us = UserService()
    resp = us.findByUsername(name)
    print("响应结果 =", resp)
    print("type(resp) =", type(resp))
