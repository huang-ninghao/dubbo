
import json

from 封装_dubbo.base_service import BaseService


class OrderSettingService(BaseService):
    def __init__(self):
        super().__init__()
        self.service_name = "OrderSettingService"

    def add(self, info):
        resp = self.dubbo_client.invoke(self.service_name, "add", info)
        if resp == "null":
            return True
        else:
            return False

    def get_Order_Setting_By_Month(self, months):
        resp = self.dubbo_client.invoke(self.service_name, "getOrderSettingByMonth",  months)
        if resp == "Failed":
            return None
        else:
            return json.loads(resp)

    def editNumberByDate(self, data):
        data["class"] = "com.itheima.pojo.OrderSetting"
        resp = self.dubbo_client.invoke(self.service_name, "getOrderSettingByMonth", data)
        if resp == "Failed":
            return False
        else:
            return True


if __name__ == '__main__':
    info = [{"orderDate": "2021-05-29 18:45:02", "number": 20}]
    os = OrderSettingService()
    resp = os.add(info)
    print("响应结果 =", resp)
    print("type(resp) =", type(resp))

    print("=" * 20)
    months = "2020-11"
    os = OrderSettingService()
    resp = os.get_Order_Setting_By_Month(months)
    print("响应结果 =", resp)
    print("type(resp) =", type(resp))

    print("=" * 20)
    date = {"orderDate": "2021-06-15 16:99:77", "number": 120}
    oss = OrderSettingService()
    resp = oss.editNumberByDate(date)
    print("响应结果 =", resp)
    print("type(resp) =", type(resp))
