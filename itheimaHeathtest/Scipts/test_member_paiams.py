##封装测试类
import unittest
from parameterized import parameterized


##把实参和预期结果参数化
from 封装_dubbo.member_service import MemberService


class TestMemberService(unittest.TestCase):
    ms = None
    @classmethod
    def setUpClass(cls) -> None:
        cls.ms=MemberService()

    @parameterized.expand([("13020210001","13020210001"),("13020218973",None),("1302021abc#",None)])
    def test_find_by_tel(self,tel,expect_result):
        resp = self.ms.find_by_tel(tel)
        if resp is None:
            self.assertEqual(None,resp)

        else:
            self.assertEqual(expect_result, resp.get("phoneNumber"))

        print("tel=", tel, "expect_result=", expect_result)


    @parameterized.expand([(["2021.5"],[5]),(["2023.13"],[40])])
    def test_find_MemberCount_By_Months(self,months,except_data):
        resp=self.ms.find_member_count_by_months(months)
        print("resp=",resp)
        self.assertEqual(except_data,resp)


